import React, { Component } from 'react';

import { ForceGraph3D } from 'react-force-graph';
import * as THREE from 'three';
import SpriteText from 'three-spritetext';
import TextData from './3DNodesData';

class Nodes3D extends Component{

    handleClick = node => {
        // Aim at node from outside it
        const distance = 40;
        const distRatio = 1 + distance/Math.hypot(node.x, node.y, node.z);
        this.fg.cameraPosition(
            { x: node.x * distRatio, y: node.y * distRatio, z: node.z * distRatio }, // new position
            node, // lookAt ({ x, y, z })
            3000  // ms transition duration
        );
        if (node.hasOwnProperty('pdf_name')){
            let PDF_NAME = node.pdf_name.split(".");
            if (PDF_NAME.length === 1){
                PDF_NAME = PDF_NAME+".pdf"
            }
            let PDF_URL = "assets/pdfs/"+PDF_NAME.join(".");
            window.open(PDF_URL, '_blank');
        }
    };

    render(){
        return(
            <ForceGraph3D
                ref={el => { this.fg = el; }}
                graphData={TextData}
                showNavInfo={false}

                nodeLabel="id"
                nodeAutoColorBy="group"
                
                onNodeClick={this.handleClick}
                enableNodeDrag = {true}

                nodeThreeObject={node => {
                    const sprite = new SpriteText(node.id);
                    sprite.color = node.color;
                    sprite.textHeight = 5;
                    sprite.position.set( 0,-15,0 )
                    const geometry = new THREE.SphereGeometry( 7 );
                    const material = new THREE.MeshLambertMaterial( {color: node.color, 
                                        transparent: true, opacity: 0.75 } );
                    const sphere = new THREE.Mesh( geometry, material );
                    let group = new THREE.Group();
                    group.add( sphere );
                    group.add( sprite );
                    return group;
                }}

                // dagMode = "zout"
                // dagLevelDistance={5}
                // d3VelocityDecay={0.09}
                // d3AlphaDecay={0.1}

                onNodeHover = {(node, nodePre) => {
                        // no state change
                        if (!node && !nodePre)
                            return;
            
                        if (node && node.fx !== 0)
                        {
                            let outlineMaterial = new THREE.MeshBasicMaterial( { color: 'cyan', side: THREE.BackSide } );
                            let outlineMesh = new THREE.Mesh( node.__threeObj.children[0].geometry, outlineMaterial );
                            // outlineMesh.position = node.__threeObj.position;
                            outlineMesh.scale.multiplyScalar(1.05);
                            node.__threeObj.add(outlineMesh);
                        }
                        if (nodePre && nodePre.fx !== 0 
                                && nodePre.__threeObj.children 
                                && nodePre.__threeObj.children.length > 0)
                        {
                            let obj = nodePre.__threeObj.children[nodePre.__threeObj.children.length - 1];
                            nodePre.__threeObj.remove(obj);
                        }
                    }
                }

                onNodeDrag = {node => console.log(node)}
                
            />
        );
    }
}

export default Nodes3D;