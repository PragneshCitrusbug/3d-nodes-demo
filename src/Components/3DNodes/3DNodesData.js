const TextData = {
    "nodes": [
        {
            "id": "3D printing Innovation",
            "group": 1,
            "pdf_name": "3D Printing innovation.pdf" 
        },
        {
            "id": "3D Scan",
            "group": 1,
            "pdf_name": "3D Printing innovation.pdf"
        },
        {
            "id": "Microsoft HoloLens",
            "group": 3
        },
        {
            "id": "Communication",
            "group": 10
        },
        {
            "id": "risks",
            "group": 4
        },
        {
            "id": "Awareness 2.0 - Threats and counter-measures",
            "group": 4, 
            "pdf_name": "AVL_under_attack-Awareness2.0.pdf"
        },
        {
            "id": "Crackdown on cracks - Compliant use of software",
            "group": 7,
            "pdf_name": "AVL_under_attack-Crackdown_on_cracks.pdf"
        },
        {
            "id": "encryption",
            "group": 4
        },
        {
            "id": "Tools",
            "group": 3
        },
        {
            "id": "Augmented Reality for Designer",
            "group": 3,
            "pdf_name": "Augumented Reality for Designer.pdf"
            
        },
        {
            "id": "3D Model",
            "group": 1
        },
        {
            "id": "Internal PM",
            "group": 2,
            "pdf_name": "Internal Project Management.pdf"
        },
        {
            "id": "Containerize me",
            "group": 6,
            "pdf_name": "Containerize me.pdf"
        },
        {
            "id": "Meetings faster , simpler , smarter",
            "group": 10,
            "pdf_name": "BI.pdf"
        },
        {
            "id": "Business Intelligence (BI)",
            "group": 5,
            "pdf_name": "BI.pdf"
        },
        {
            "id": "Development Team",
            "group": 2
        },
        {
            "id": "Endpoint security & Client compliance",
            "group": 9,
            "pdf_name": "AVL_under_attack-Endpoint_Protection_and_Client_Compliance.pdf"
        },
        {
            "id": "Digitalize it!",
            "group": 8,
            "pdf_name": "WorkflowsinPTE.pdf"
        },
        {
            "id": "Management Dashboards",
            "group": 19,
            "pdf_name": "Management Dashboards.pdf"
        },
        {
            "id": "Make your cloud shiny",
            "group": 20,
            "pdf_name": "AVL and the Cloud - a successful partnership.pdf"
        },
        {
            "id": "Development",
            "group": 11
        },
        {
            "id": "Interfaces",
            "group": 11
        },
        {
            "id": "Project Organization",
            "group": 8
        },
        {
            "id": "Communication",
            "group": 2
        },
        {
            "id": "Performance",
            "group": 11
        },
        {
            "id": "External collaboration",
            "group": 15
        },
        {
            "id": "Digitalization",
            "group": 8
        },
        {
            "id": "analytics",
            "group": 16
        },
        {
            "id": "security",
            "group": 16
        },
        {
            "id": "assessment",
            "group": 9
        },
        {
            "id": "private",
            "group": 9
        },
        {
            "id": "Information leak",
            "group": 9
        },
        {
            "id": "Change",
            "group": 2
        },
        {
            "id": "Software",
            "group": 13
        },
        {
            "id": "Cloud Provider",
            "group": 20
        },
        {
            "id": "IaaS",
            "group": 20
        },
        {
            "id": "Design",
            "group": 3
        },
        {
            "id": "Caas",
            "group": 20
        },
        {
            "id": "UI",
            "group": 18
        },
        {
            "id": "Lightning",
            "group": 18
        },
        {
            "id": "Subscriptions",
            "group": 20
        },
        {
            "id": "Schedule",
            "group": 10
        },
        {
            "id": "Testing",
            "group": 13
        },
        {
            "id": "NEW Salesforce Lightning Experience",
            "group": 18,
            "pdf_name": "Salesforce Lightning.pdf"
        },
        {
            "id": "Analyzation",
            "group": 17
        },
        {
            "id": "SAP",
            "group": 5
        },
        {
            "id": "proWizard",
            "group": 17,
            "pdf_name": "proWizard.pdf"
        },
        {
            "id": "work, share, collaborate, archive",
            "group": 11,
            "pdf_name": "work share collaborate.pdf"
        },
        {
            "id": "Salesforce",
            "group": 18
        },
        {
            "id": "Share",
            "group": 10
        },
        {
            "id": "Management",
            "group": 17
        },
        {
            "id": "Project Quality",
            "group": 17
        },
        {
            "id": "Visualisation",
            "group": 14
        },
        {
            "id": "Apps",
            "group": 3
        },
        {
            "id": "Global",
            "group": 14
        },
        {
            "id": "Software",
            "group": 14
        },
        {
            "id": "Workflow",
            "group": 13
        },
        {
            "id": "Product Owner",
            "group": 13
        },
        {
            "id": "Windchill 11",
            "group": 13,
            "pdf_name": "Windchill 11.pdf"
        },
        {
            "id": "Windchill",
            "group": 15
        },
        {
            "id": "PTE - Engineering",
            "group": 15,
            "pdf_name":"PTE-Engineering.pdf"
        },
        {
            "id": "dev.community.create (AVL)",
            "group": 12,
            "pdf_name": "Dev Community.pdf"
        },
        {
            "id": "Machine Learning",
            "group": 5
        },
        {
            "id": "Measure",
            "group": 5
        },
        {
            "id": "attack",
            "group": 4
        },
        {
            "id": "Apps",
            "group": 10
        },
        {
            "id": "3D",
            "group": 3
        },
        {
            "id": "vulnerability",
            "group": 4
        },
        {
            "id": "illegal software",
            "group": 7
        },
        {
            "id": "Processes",
            "group": 2
        },
        {
            "id": "Quality Management",
            "group": 15
        },
        {
            "id": "Organization Development",
            "group": 19
        },
        {
            "id": "Opportunities",
            "group": 19
        },
        {
            "id": "AVL Strategy",
            "group": 19
        },
        {
            "id": "Decisions",
            "group": 19
        },
        {
            "id": "Monitoring",
            "group": 7
        },
        {
            "id": "Powertrain Database - PTDB",
            "group": 16,
            "pdf_name":"PTDB What is it.pdf"
        },
        {
            "id": "cyber security",
            "group": 7
        },
        {
            "id": "SaaS",
            "group": 20
        },
        {
            "id": "Cloud",
            "group": 6
        },
        {
            "id": "Management",
            "group": 6
        },
        {
            "id": "Security",
            "group": 6
        },
        {
            "id": "Powertrain Elements",
            "group": 16
        },
        {
            "id": "Support",
            "group": 11
        },
        {
            "id": "Sign it Digital now!",
            "group": 14,
            "pdf_name": "Sign it digital now.pdf"
        }
    ],
    "links": [
        {
            "source": "Make your cloud shiny",
            "target": "3D printing Innovation",
            "value": 2.0
        },
        {
            "source": "3D printing Innovation",
            "target": "3D Scan",
            "value": 2.0
        },
        {
            "source": "Augmented Reality for Designer",
            "target": "Microsoft HoloLens",
            "value": 2.0
        },
        {
            "source": "Awareness 2.0 - Threats and counter-measures",
            "target": "risks",
            "value": 2.0
        },
        {
            "source": "Crackdown on cracks - Compliant use of software",
            "target": "Awareness 2.0 - Threats and counter-measures",
            "value": 2.0
        },
        {
            "source": "Augmented Reality for Designer",
            "target": "Awareness 2.0 - Threats and counter-measures",
            "value": 2.0
        },
        {
            "source": "Endpoint security & Client compliance",
            "target": "Awareness 2.0 - Threats and counter-measures",
            "value": 2.0
        },
        {
            "source": "Endpoint security & Client compliance",
            "target": "Crackdown on cracks - Compliant use of software",
            "value": 2.0
        },
        {
            "source": "Awareness 2.0 - Threats and counter-measures",
            "target": "encryption",
            "value": 2.0
        },
        {
            "source": "Augmented Reality for Designer",
            "target": "Tools",
            "value": 2.0
        },
        {
            "source": "Internal PM",
            "target": "Tools",
            "value": 2.0
        },
        {
            "source": "Internal PM",
            "target": "Augmented Reality for Designer",
            "value": 2.0
        },
        {
            "source": "3D printing Innovation",
            "target": "3D Model",
            "value": 2.0
        },
        {
            "source": "3D printing Innovation",
            "target": "Internal PM",
            "value": 2.0
        },
        {
            "source": "Make your cloud shiny",
            "target": "Containerize me",
            "value": 2.0
        },
        {
            "source": "dev.community.create (AVL)",
            "target": "Containerize me",
            "value": 2.0
        },
        {
            "source": "work, share, collaborate, archive",
            "target": "Meetings faster , simpler , smarter",
            "value": 2.0
        },
        {
            "source": "Internal PM",
            "target": "Development Team",
            "value": 2.0
        },
        {
            "source": "work, share, collaborate, archive",
            "target": "Management Dashboards",
            "value": 2.0
        },
        {
            "source": "Sign it Digital now!",
            "target": "Management Dashboards",
            "value": 2.0
        },
        {
            "source": "work, share, collaborate, archive",
            "target": "Make your cloud shiny",
            "value": 2.0
        },
        {
            "source": "dev.community.create (AVL)",
            "target": "Make your cloud shiny",
            "value": 2.0
        },
        {
            "source": "Sign it Digital now!",
            "target": "Make your cloud shiny",
            "value": 2.0
        },
        {
            "source": "work, share, collaborate, archive",
            "target": "Development",
            "value": 2.0
        },
        {
            "source": "dev.community.create (AVL)",
            "target": "Development",
            "value": 2.0
        },
        {
            "source": "work, share, collaborate, archive",
            "target": "Interfaces",
            "value": 2.0
        },
        {
            "source": "dev.community.create (AVL)",
            "target": "Interfaces",
            "value": 2.0
        },
        {
            "source": "Digitalize it!",
            "target": "Project Organization",
            "value": 2.0
        },
        {
            "source": "Meetings faster , simpler , smarter",
            "target": "Communication",
            "value": 2.0
        },
        {
            "source": "proWizard",
            "target": "Communication",
            "value": 2.0
        },
        {
            "source": "work, share, collaborate, archive",
            "target": "Communication",
            "value": 2.0
        },
        {
            "source": "dev.community.create (AVL)",
            "target": "Communication",
            "value": 2.0
        },
        {
            "source": "work, share, collaborate, archive",
            "target": "Performance",
            "value": 2.0
        },
        {
            "source": "dev.community.create (AVL)",
            "target": "Performance",
            "value": 2.0
        },
        {
            "source": "PTE - Engineering",
            "target": "External collaboration",
            "value": 2.0
        },
        {
            "source": "Digitalize it!",
            "target": "Digitalization",
            "value": 2.0
        },
        {
            "source": "Powertrain Database - PTDB",
            "target": "analytics",
            "value": 2.0
        },
        {
            "source": "Powertrain Database - PTDB",
            "target": "security",
            "value": 2.0
        },
        {
            "source": "Endpoint security & Client compliance",
            "target": "assessment",
            "value": 2.0
        },
        {
            "source": "Endpoint security & Client compliance",
            "target": "private",
            "value": 2.0
        },
        {
            "source": "Endpoint security & Client compliance",
            "target": "Information leak",
            "value": 2.0
        },
        {
            "source": "Internal PM",
            "target": "Change",
            "value": 2.0
        },
        {
            "source": "Digitalize it!",
            "target": "Change",
            "value": 2.0
        },
        {
            "source": "Make your cloud shiny",
            "target": "Cloud Provider",
            "value": 2.0	
        },
        {
            "source": "Make your cloud shiny",
            "target": "IaaS",
            "value": 2.0
        },
        {
            "source": "Augmented Reality for Designer",
            "target": "Design",
            "value": 2.0
        },
        {
            "source": "Make your cloud shiny",
            "target": "Caas",
            "value": 2.0
        },
        {
            "source": "NEW Salesforce Lightning Experience",
            "target": "UI",
            "value": 2.0
        },
        {
            "source": "NEW Salesforce Lightning Experience",
            "target": "Lightning",
            "value": 2.0
        },
        {
            "source": "Make your cloud shiny",
            "target": "Subscriptions",
            "value": 2.0
        },
        {
            "source": "Meetings faster , simpler , smarter",
            "target": "Schedule",
            "value": 2.0
        },
        {
            "source": "Windchill 11",
            "target": "Testing",
            "value": 2.0
        },
        {
            "source": "PTE - Engineering",
            "target": "Testing",
            "value": 2.0
        },
        {
            "source": "proWizard",
            "target": "Analyzation",
            "value": 2.0
        },
        {
            "source": "Business Intelligence (BI)",
            "target": "SAP",
            "value": 2.0
        },
        {
            "source": "PTE - Engineering",
            "target": "proWizard",
            "value": 2.0
        },
        {
            "source": "Powertrain Database - PTDB",
            "target": "proWizard",
            "value": 2.0
        },
        {
            "source": "NEW Salesforce Lightning Experience",
            "target": "Salesforce",
            "value": 2.0
        },
        {
            "source": "Meetings faster , simpler , smarter",
            "target": "Share",
            "value": 2.0
        },
        {
            "source": "proWizard",
            "target": "Project Quality",
            "value": 2.0
        },
        {
            "source": "Sign it Digital now!",
            "target": "Visualisation",
            "value": 2.0
        },
        {
            "source": "Sign it Digital now!",
            "target": "Apps",
            "value": 2.0
        },
        {
            "source": "Sign it Digital now!",
            "target": "Global",
            "value": 2.0
        },
        {
            "source": "Sign it Digital now!",
            "target": "Software",
            "value": 2.0
        },
        {
            "source": "Windchill 11",
            "target": "Workflow",
            "value": 2.0
        },
        {
            "source": "Windchill 11",
            "target": "Product Owner",
            "value": 2.0
        },
        {
            "source": "PTE - Engineering",
            "target": "Windchill",
            "value": 2.0
        },
        {
            "source": "work, share, collaborate, archive",
            "target": "dev.community.create (AVL)",
            "value": 2.0
        },
        {
            "source": "Business Intelligence (BI)",
            "target": "Machine Learning",
            "value": 2.0
        },
        {
            "source": "Awareness 2.0 - Threats and counter-measures",
            "target": "attack",
            "value": 2.0
        },
        {
            "source": "Augmented Reality for Designer",
            "target": "3D",
            "value": 2.0
        },
        {
            "source": "Awareness 2.0 - Threats and counter-measures",
            "target": "vulnerability",
            "value": 2.0
        },
        {
            "source": "Crackdown on cracks - Compliant use of software",
            "target": "illegal software",
            "value": 2.0
        },
        {
            "source": "Internal PM",
            "target": "Processes",
            "value": 2.0
        },
        {
            "source": "PTE - Engineering",
            "target": "Quality Management",
            "value": 2.0
        },
        {
            "source": "Management Dashboards",
            "target": "Organization Development",
            "value": 2.0
        },
        {
            "source": "Management Dashboards",
            "target": "AVL Strategy",
            "value": 2.0
        },
        {
            "source": "Management Dashboards",
            "target": "Decisions",
            "value": 2.0
        },
        {
            "source": "Crackdown on cracks - Compliant use of software",
            "target": "Monitoring",
            "value": 2.0
        },
        {
            "source": "PTE - Engineering",
            "target": "Powertrain Database - PTDB",
            "value": 2.0
        },
        {
            "source": "Crackdown on cracks - Compliant use of software",
            "target": "cyber security",
            "value": 2.0
        },
        {
            "source": "Make your cloud shiny",
            "target": "SaaS",
            "value": 2.0
        },
        {
            "source": "Containerize me",
            "target": "Cloud",
            "value": 2.0
        },
        {
            "source": "Containerize me",
            "target": "Management",
            "value": 2.0
        },
        {
            "source": "Containerize me",
            "target": "Security",
            "value": 2.0
        },
        {
            "source": "Powertrain Database - PTDB",
            "target": "Powertrain Elements",
            "value": 2.0
        },
        {
            "source": "work, share, collaborate, archive",
            "target": "Support",
            "value": 2.0
        },
        {
            "source": "dev.community.create (AVL)",
            "target": "Support",
            "value": 2.0
        },
        {
            "source": "Sign it Digital now!",
            "target": "Support",
            "value": 2.0
        },
        {
            "source": "work, share, collaborate, archive",
            "target": "Sign it Digital now!",
            "value": 2.0
        },
        {
            "source": "dev.community.create (AVL)",
            "target": "Sign it Digital now!",
            "value": 2.0
        }
    ]
}
export default TextData;