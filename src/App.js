import React from 'react';
import Nodes3D from './Components/3DNodes/3DNodes';
import './App.css';

function App() {
  return (
    <div>
      <Nodes3D />
    </div>
  );
}

export default App;
